
//3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos',
	{method:'GET'})
.then((response)=>response.json())
.then((json) => console.log(json))

//4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

fetch('https://jsonplaceholder.typicode.com/todos',
	{method:'GET'})
.then((response)=>response.json())
.then((json) => 
	json.map((x)=>console.log(x.title)))


//5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1',
	{method:'GET',
	})
.then((response)=>response.json())
.then((json) => console.log(json))

//6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

fetch('https://jsonplaceholder.typicode.com/todos/1',
	{method:'GET',
	})
.then((response)=>response.json())
.then((json) => console.log(json.title+" "+json.completed))


//7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos', {
	//Sets the method of the "request" object to "POST"
	//Default method is GET
	method: 'POST',
	//Sets the header data of the "request" object to be sent to the backend
	headers: {
		'Content-Type': 'application/json'
	},
	//sets the content/body data of the "request" object to be sent to the backend
	body: JSON.stringify({
		title: 'try',
		body: 'hirap',
		userID: 1

	}),

})
.then((response)=> response.json())
.then((json)=> console.log(json))

//8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method:'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated Post',
		body: 'Hello Again!',
		userId: 1
	}),
})
.then((response)=> response.json())
.then((json)=> console.log(json))

/*
9. Update a to do list item by changing the data structure to contain the following properties:
- Title
- Description
- Status
- Date Completed
- User ID
*/

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method:'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated Title',
		description: 'Updated Description',
		status: 'Updated Status',
		dateCompleted: '01-04-2022',
		userID: 123
	}),
})
.then((response)=> response.json())
.then((json)=> console.log(json))

//10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method:'PATCH',
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Patch Post'
	})
})
.then((response)=> response.json())
.then((json)=> console.log(json))

//11. Update a to do list item by changing the status to complete and add a date when the status was changed.

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method:'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated Title',
		description: 'Updated Description',
		status: 'Complete',
		dateCompleted: '01-06-2022',
		userID: 123
	}),
})
.then((response)=> response.json())
.then((json)=> console.log(json))

fetch('https://jsonplaceholder.typicode.com/todos/1',
	{method:'DELETE'})










